extern string_equals
global find_word

find_word:
;rdi - указатель на 0-мировануую
;rsi - указатель на начало словаря
push rbx
push rbp
mov rbx, rdi
mov rbp, rsi
.loop:
test rsi, rsi
jz .lose

add rbp, 8 ;get metka
mov rsi, rbp
mov rdi, rbx
call string_equals
sub rbp, 8

test rax, rax
jnz .win

mov rbp, [rbp]
jmp .loop
.win:
mov rax, rbp
jmp .exit
.lose:
mov rax, 0
.exit:
pop rbp
pop rbx
ret
