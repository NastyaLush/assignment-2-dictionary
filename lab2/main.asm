%include "words.inc"
;%include "colon.inc"
%include "dist.inc"
%include "lib.inc"

%define size 256
section .rodata
error: db "Nothing found", 0
error_read: db "read error", 0

section .bss
buffer: resb size

section .text
global _start
_start:

mov rdi, buffer
mov rsi, size

call read_word
mov rcx, rdx
mov rdi, rax

test rax, rax
jz .error_read

.find:
    mov rsi, head_url
    call find_word

    test rax, rax
    jz .error

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    lea rdi, [rdi+rax+1]
    call print_string
    call print_newline
    jmp .exit

.error:
mov rdi, error
jmp .print_error
.error_read:
mov rdi, error_read
.print_error:
call print_error
call print_newline
.exit:
call exit
